// Практичні завдання
// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.


function isPalindrome(str) {
    str = str.toLowerCase();
    for (let i = 0, j = str.length - 1; i < j; i++, j--) {
        if (str[i] !== str[j]) {
        return console.log(false)
        }
    }
    return console.log(true)
}
isPalindrome('Alla')


// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// // Рядок коротше 20 символів

function strLength (str, num) {
    if(str.length === num){
        return console.log(true)
    }else {
        return console.log(false)
    }
}
strLength('julia', 5)


// 3. Створіть функцію, яка визначає скільки повних років користувачу.
//     Отримайте дату народження користувача через prompt.
//     Функція повина повертати значення повних років на дату виклику функцію.

function calculateAge(birthDate) {
    let birthDateObj = new Date(birthDate);
    let currentDate = new Date();
    let age = currentDate.getFullYear() - birthDateObj.getFullYear();
    let monthDiff = currentDate.getMonth() - birthDateObj.getMonth();

    if (monthDiff < 0 || (monthDiff === 0 && currentDate.getDate() < birthDateObj.getDate())) {
        age--;
    }
    return age;
}

let userBirthDate = prompt("Введіть вашу дату народження у форматі YYYY-MM-DD:");

console.log("Ваш вік:", calculateAge(userBirthDate), "років");

